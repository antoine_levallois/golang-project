package main
import (
	"net/http"
	"fmt"
	"time"

)

type Welcome struct {
	Name string
	Time string
}


func main() {
	
	welcome := Welcome{"Anonymous", time.Now().Format(time.Stamp)}



	http.Handle("/myPath/", //final url can be anything
		http.StripPrefix("/static/",
			http.FileServer(http.Dir("static")))) //Go looks in the relative static directory first, then matches it to a
			


	http.HandleFunc("/" , func(w http.ResponseWriter, r *http.Request) {

				if name := r.FormValue("name"); name != "" {
			welcome.Name = name;
		}
		
		
	})


	fmt.Println(http.ListenAndServe(":8080", nil));
}
